# -*- -*- -*-  -*-  -*-  -*-  -*-  -*-  
# -*- coding: utf-8 -*-                                         
#  @Time    : 2022/08/20
#  @File    : send_requests.py
#  @Desc    : 这里存放的是封装好的请求方法
# -*- -*- -*-  -*-  -*-  -*-  -*-  -*-
import requests
from Tools.log_output import get_logger
from Tools.data_modify import allure_step,allure_step_no

logger = get_logger('logger')


class BaseRequest(object):
    session = None

    @classmethod
    def get_session(cls):
        """
        单例模式保证测试过程中使用的都是一个session对象
        :return:
        """
        if cls.session is None:
            cls.session = requests.Session()
        return cls.session

    @classmethod
    def send_request(cls,url,method, parametric_key,headers=None,data=None,file=None):
        """处理case数据，转换成可用数据发送请求
        :param case: 读取出来的每一行用例内容，可进行解包
        return: 响应结果， 预期结果
        """
        # 发送请求
        response = cls.send_api(url, method, parametric_key, headers, data, file)
        return response

    @classmethod
    def send_api(cls,url,method,parametric_key,headers=None,data=None,file=None) -> dict:
        """
        :param method: 请求方法
        :param url: 请求url
        :param parametric_key: 入参关键字， params(查询参数类型，明文传输，一般在url?参数名=参数值), data(一般用于form表单类型参数)
        json(一般用于json类型请求参数)
        :param data: 参数数据，默认等于None
        :param file: 文件对象
        :param header: 请求头
        :return: 返回res对象
        """
        session = cls.get_session()

        if parametric_key == 'params':
            res = session.request(
                method=method,
                url=url,
                params=data,
                headers=headers)
        elif parametric_key == 'data':
            res = session.request(
                method=method,
                url=url,
                data=data,
                files=file,
                headers=headers)
        elif parametric_key == 'json':
            res = session.request(
                method=method,
                url=url,
                json=data,
                files=file,
                headers=headers)
        else:
            raise ValueError(
                '可选关键字为params, json, data')
        response = res.json()
        logger.info("\n\n*************** 用例执行记录 *******************")
        logger.info(f'【{method}】 【{res.status_code}】：URL >>> {url}')
        logger.info(f"【请求Header 】：{headers}")
        logger.info(f"【请求Body   】：{data}")
        logger.info(f"【请求file   】：{file}")
        logger.info(f"【响应返回   】：{response}\n")
        allure_step_no(f'响应耗时(s): {res.elapsed.total_seconds()}')
        allure_step('请求Body：', data)
        allure_step_no(f"请求Header：{headers}")
        allure_step('响应结果：', response)
        return response