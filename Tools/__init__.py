# -*- coding: utf-8 -*- 
# @Time      :  2022/8/12 16:51
# @Author    :  PythonKimo
# @File      :  __init__.py.py
# @Desc      :  
# -*- -*- -*- -*- -*- -*-
# import time
#
# # 使用time
# timeStamp = 1660722215
# timeArray = time.localtime(timeStamp)
# rel_time = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
# print('\n' + '时间戳:【{}】，格式化后的时间为【{}】'.format(timeStamp,rel_time))
