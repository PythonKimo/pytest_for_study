# -*- coding: utf-8 -*- 
# @Time      :  2022/8/12 16:51
# @Author    :  PythonKimo
# @File      :  data_modify.py
# @Desc      :  
# -*- -*- -*- -*- -*- -*-
import os
import allure
import json

# 设置报告窗口的标题
def data_replace(filepath,old_params,new_params):
    # filepath = os.path.join(os.path.dirname(__file__),r"../Pywebreport_Result_and_Report/report.html")
    # 定义为只读模型，并定义名称为f
    with open(filepath, 'r+', encoding="utf-8") as f:
        # 读取当前文件的所有内容
        all_the_lines = f.readlines()
        f.seek(0)
        f.truncate()
        # 循环遍历每一行的内容，将 "旧文案" 全部替换为 → (新文案)
        for line in all_the_lines:
            line = line.replace(old_params, new_params)
            f.write(line)
        # 关闭文件
        f.close()

def allure_step(step: str, var: str) -> None:
    """
    :param step: 步骤及附件名称
    :param var: 附件内容
    """
    with allure.step(step):
        allure.attach(
            json.dumps(
                var,
                ensure_ascii=False,
                indent=4),
            step,
            allure.attachment_type.JSON)

def allure_step_no(step: str):
    """
    无附件的操作步骤
    :param step: 步骤名称
    :return:
    """
    with allure.step(step):
        pass
