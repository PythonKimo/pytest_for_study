# -*- coding: utf-8 -*-
# @Time      :  2022/8/16 15:31
# @Author    :  PythonKimo
# @File      :  log_output.py
# @Desc      :  封装的log日志存储及输出方法
# -*- -*- -*- -*- -*- -*-
import logging
import os

def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(10)   # 设置总日志等级

    format = logging.Formatter(fmt='%(message)s')  # 日志格式

    cli_handler = logging.StreamHandler()  # 输出到屏幕的日志处理器

    log_result = os.path.join(os.path.dirname(__file__),r'../Testcases_Running_Log/case_run.log')
    file_handler = logging.FileHandler(filename=log_result, mode='w', encoding='utf-8')  # 输出到文件的日志处理器，mode默认是’a’，即添加到文件末尾，‘w’是覆盖写入

    cli_handler.setFormatter(format)  # 设置屏幕日志格式
    file_handler.setFormatter(format)  # 设置文件日志格式

    cli_handler.setLevel(logging.INFO)  # 设置屏幕日志等级, 可以大于日志记录器设置的总日志等级
    # file_handler.setLevel(logging.DEBUG)  # 不设置默认使用logger的等级

    logger.handlers.clear()  # 清空已有处理器, 避免继承了其他logger的已有处理器
    logger.addHandler(file_handler)  # 将文件日志处理器添加到logger

    ''' 该行主要控制了控制台的日志输出，但是会导致报告中日志数据重复，在 run文件执行时保持注释状态即可 '''
    # logger.addHandler(cli_handler)  # 将屏幕日志处理器添加到logger
    return logger


