# -*- -*- -*-  -*-  -*-  -*-  -*-  -*-  
# -*- coding: utf-8 -*-
#  @创建日期 : 2022/08/20
#  @文件名称 : app.py.py
#  @创 建 者 : PythonKimo
# -*- -*- -*-  -*-  -*-  -*-  -*-  -*-
# coding=gbk
from flask import request, Flask, jsonify

app = Flask(__name__)

@app.route('/api/v1/addUser', methods=['POST'])  # 括号里写的是访问时的路径地址和请求方式
def addUser():
    return jsonify({
        "GatewayStatus": 1,
        "GatewayMessage": "成功",
        "DetailedStatus": 1,
        "DetailedMessage": "新增成功",
        "Data": {
            "id": 1,
            "name": "张三",
            "sex": "男",
            "age": 18,
            "loginId": "test"
        }
    })

@app.route('/api/v1/getUserInfo', methods=['GET'])  # 括号里写的是访问时的路径地址和请求方式
def getUserInfo():
    return jsonify({
        "GatewayStatus": 1,
        "GatewayMessage": "成功",
        "DetailedStatus": 1,
        "DetailedMessage": "查询成功",
        "Data": {
            "id": 1,
            "name": "张三",
            "sex": "男",
            "age": 18,
            "loginId": "test"
        }
    })

@app.route('/api/v1/deleteUser', methods=['DELETE'])  # 括号里写的是访问时的路径地址和请求方式
def deleteUser():
    return jsonify({
        "GatewayStatus": 1,
        "GatewayMessage": "成功",
        "DetailedStatus": 1,
        "DetailedMessage": "删除成功"
    })

@app.route('/api/v1/signLogin', methods=['POST'])  # 括号里写的是访问时的路径地址和请求方式
def userLogin():
    return jsonify({
        "GatewayStatus": 1,
        "GatewayMessage": "成功",
        "DetailedStatus": 1,
        "DetailedMessage": "登录成功",
        "Data": {
            "token": "token2022",
            "id": 1,
            "name": "张三",
            "sex": "男",
            "age": 18,
            "loginId": "test"
        }
    })

@app.route('/api/v1/RefreshToken', methods=['POST'])  # 括号里写的是访问时的路径地址和请求方式
def refreshToken():
    return jsonify({
        "GatewayStatus": 1,
        "GatewayMessage": "成功",
        "DetailedStatus": 1,
        "DetailedMessage": "刷新成功",
        "Data": {
            "token": "token20211012",
            "id": 1,
            "name": "张三",
            "sex": "男",
            "age": 18,
            "loginId": "test"
        }
    })

@app.route('/api/v1/signout', methods=['POST'])  # 括号里写的是访问时的路径地址和请求方式
def signOut():
    return jsonify({
        "GatewayStatus": 1,
        "GatewayMessage": "成功",
        "DetailedStatus": 1,
        "DetailedMessage": "用户退出"
    })


if __name__ == '__main__':
    app.run( host='127.0.0.1')
# 启动服务,host写成0.0.0.同一个局域网可以访问


