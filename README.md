### 这里对整个项目文件夹写一点描述

### ⭐Allure_Report 文件夹
    这里主要存放通过Allure生成的测试报告
![输入图片说明](API_Mock_By_Flask/allure%E6%B5%8B%E8%AF%95%E6%8A%A5%E5%91%8A.jpg)

### ⭐Allure_Result 文件夹
    这里主要存放通过Allure生成的测试报告的json内容

### ⭐Api 文件夹
    1. 这里主要是将 requests 方法封装了一下，便于在 services 中统一调用
    2. 而且在调用请求方法时，会按照我自定义的格式进行日志的打印

### ⭐API_Mock_By_Flask 文件夹
    1. 这里主要是存放 mock 的假接口，用到的是 flask 
    2. 不过是最初级的写法，没有任何的入参规则校验，但是用来学习已经够用了
    3. ⭐注意：【单独执行用例文件或者 run 所有用例前，先运行 app.py ,不然接口是调不通的】

### ⭐Pywebreport_Result_and_Report 文件夹
    1. 这里主要是存放生成的测试报告，用到了网上一位大佬（迭代开发中）的开源测试报告库 PyWebReport
    2. 原文链接地址：https://yongchin.xyz/posts/dev/20220721
    3. Github地址：https://github.com/yongchin0821/pywebreport
    4. 觉得不错的话，可以去点个 star ！
![输入图片说明](API_Mock_By_Flask/pywebreport%E6%B5%8B%E8%AF%95%E6%8A%A5%E5%91%8A.jpg)

### ⭐Services 文件夹
    1. 这里主要是存放需要编写的接口，并封装成方法，便于在 testcases 编写用例时调用

### ⭐testcases 文件夹
    1. 这里就是编写和存储测试用例的地方

### ⭐Testcases_Running_Log 文件夹
    1. 这里主要是将用例执行的日志存储并写入 .log 的文件中

### ⭐Tools 文件夹
    1. 这里主要是存放一些工具类方法，比如我这边我这边写了 data_modify 和 log_output 
    2. data_modify：封装了一个文件内容查找并替换的方法（主要是来DIY测试报告的展示）
    3. log_output：封装了一个输入log日志的方法，便于控制台展示和 .log 文件存储执行日志【记得看注释提示内容】

### ⭐run_allure.py 文件
    1. 通过 allure 执行所有 test 打头的测试用例，并生成测试报告

### ⭐run_pywebreport 文件
    通过 pywebreport 执行所有 test 打头的测试用例，并生成测试报告