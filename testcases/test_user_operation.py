# -*- coding: utf-8 -*- 
# @Time      :  2022/8/19 11:17
# @Author    :  PythonKimo
# @File      :  test_user_operation.py
# @Desc      :  
# -*- -*- -*- -*- -*- -*-
import time
import allure
from Services import user_managment
from Tools.log_output import get_logger
import pytest

"""
@allure.severity(): severity_level 枚举
    blocker： 阻塞缺陷(功能未实现，无法下一步)
    critical：严重缺陷(功能点缺失)
    normal：  一般缺陷(边界情况，格式错误)
    minor：   次要缺陷(界面错误与ui需求不符)
    trivial： 轻微缺陷(必须项无提示，或者提示不规范)
"""

logger = get_logger('logger')

# 这里的 fixture 方法均为数据构建或前置条件
@pytest.fixture(scope="function")
def user_id():
    new_user_info =  dict({
        'user_name': '张三',
        'sex': '男',
        'login_id': 'test1'
    })
    res = user_managment.user_info_operation.add_user(**new_user_info)
    assert res["DetailedMessage"] == "新增成功"
    user_id = res["Data"]["id"]
    # yield 替代return，返回新增用户返回的用户ID
    yield user_id

    # 当 user_id 使用完毕后，调用删除用户接口，防止构建大量测试垃圾数据
    res = user_managment.user_info_operation.delete_user(id=user_id)
    assert res["DetailedMessage"] == "删除成功"


# 这里的 fixture 方法均为数据构建或前置条件
@pytest.fixture(scope="function")
# 为了能够登录成功，需要依赖用户新增成功
def token(user_id):
    user_info = dict({
        'login_id': 'test1',
        'pass_word': '123456'
    })
    # 调用用户登录接口
    res = user_managment.user_info_operation.user_sign_login(**user_info)
    assert res["DetailedMessage"] == "登录成功"
    token = res["Data"]["token"]
    return token

@allure.feature("服务名称：用户管理服务")
@allure.story("用例模块：用户增删改查相关模块")
class Test_User_Operation:

    @allure.title("用例标题：通过正确的用户ID，查询用户信息")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.description("用例描述：传入正确的用户ID，调用用户信息查询接口，断言响应结果")
    def test_001_query_user_info(self,user_id):
        '''查询用户信息'''
        res = user_managment.user_info_operation.get_user_info(id=user_id)
        time.sleep(0.5) # 为了看请求时间
        assert res["DetailedMessage"] == "查询成功"
        assert len(res["Data"]) >= 1

    @allure.title("用例标题：刷新用户Token有效时常")
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.description("用例描述：传入用户的Token，调用有效期刷新接口，断言响应结果")
    def test_002_user_token_refresh(self,token):
        '''刷新用户token有效期'''
        time.sleep(0.5) # 为了看请求时间
        res = user_managment.user_info_operation.user_token_refresh(token=token)
        assert res["DetailedMessage"] == "刷新成功"
        assert res["GatewayMessage"] == "成功"

    @allure.title("用例标题：用户账号登出")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.description("重置用户Token，调用用户登出查询接口，断言相应结果")
    def test_003_user_logout(self,token):
        '''用户登出，将token有效期过期'''
        time.sleep(0.5) # 为了看请求时间
        res = user_managment.user_info_operation.user_signout(token=token)
        assert res["DetailedMessage"] == "用户退出"
        assert res["GatewayMessage"] == "成功"
