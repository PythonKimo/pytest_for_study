# -*--*- coding: utf-8 -*--*-
# @Time    : 2021/10/16 18:52
# @Author  : PythonKimo
# @File    : test_userManage.py
# -*-*-*-*-*-*-*-*-*-*-*-*-*-
import time
import pytest
import allure
from Tools.log_output import get_logger

logger = get_logger('logger')

@allure.feature("服务名称：Mock的接口服务")
@allure.story("用例模块：Mock的测试用例模块")
class Test_User:

    @allure.title("用例标题：mock测试用例_第1条")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.description("用例描述：第一条用例，断言 1 == 1")
    def test_001_first_case(self):
        '''第1条测试用例'''
        logger.info("为了在log文件存放一点自定义的日志，所以写了这行代码")
        assert 1 == 1

    @allure.title("用例标题：mock测试用例_第2条")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.description("用例描述：第二条用例，断言 2 == 3")
    def test_002_second_case(self):
        '''第2条测试用例'''
        assert 2 == 3

    @allure.title("用例标题：mock测试用例_第3条")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.description("用例描述：第三条用例，故意sleep 2秒，为了看测试报告数据展示")
    def test_003_third_case(self):
        '''第3条测试用例'''
        time.sleep(2)
        pass

    @allure.title("用例标题：mock测试用例_第4条")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.description("用例描述：第条四用例，故意跳过，为了看测试报告数据展示")
    @pytest.mark.skip("暂时跳过不执行")
    def test_004_fourth_case(self):
        '''第4条测试用例'''
        time.sleep(1)
        assert 1 == 3

if __name__ == '__main__':
    pytest.main(["-sv","test_mock.py"])
