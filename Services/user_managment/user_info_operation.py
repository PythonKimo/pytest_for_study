# -*- coding: utf-8 -*- 
# @Time      :  2022/8/19 10:50
# @Author    :  PythonKimo
# @File      :  user_info_operation.py
# @Desc      :  封装了测试用例所有调用接口方法
# -*- -*- -*- -*- -*- -*-
from Api.send_requests import BaseRequest
import Services


def add_user(user_name=None,sex=None,login_id=None, **kwargs):
    """
    输入用户信息及登录账号，创建新用户
    :param user_name: 用户姓名
    :param sex: 用户性别
    :param login_id: 用户登录ID
    :param kwargs:
    :return: 返回创建用户信息及创建状态
    """
    url = Services.ip_port + '/api/v1/addUser'
    method = 'POST'
    parametric_key = 'json'
    headers = {'Content-Type': 'application/json'}
    data = {
        'userName': user_name,
        'sex': sex,
        'loginId': login_id
    }

    return BaseRequest.send_request(method=method, url=url, parametric_key=parametric_key,data=data, headers=headers)


def get_user_info(id=None,**kwargs):
    """
    根据用户id，查询用户信息
    :param id: 数据库存储的用户id
    :param kwargs:
    :return: 响应返回用户的详细信息
    """
    url = Services.ip_port + '/api/v1/getUserInfo'
    method = 'GET'
    parametric_key = 'params'
    headers = {'Content-Type': 'application/json'}
    data = {
        'id': id
    }

    return BaseRequest.send_request(method=method, url=url, parametric_key=parametric_key,data=data, headers=headers)

def delete_user(id=None,**kwargs):
    """
    根据用户id，查询用户信息
    :param id: 数据库存储的用户id
    :param kwargs:
    :return: 用户调用删除接口返回的操作状态
    """
    url = Services.ip_port + '/api/v1/deleteUser'
    method = 'DELETE'
    parametric_key = 'json'
    headers = {'Content-Type': 'application/json'}
    data = {
        'id': id
    }

    return BaseRequest.send_request(method=method, url=url, parametric_key=parametric_key,data=data, headers=headers)


def user_sign_login(login_id=None,password=None,**kwargs):
    """
    用户使用账号+密码进行登录
    :param login_id: 用户的登录ID
    :param password: 用户的密码
    :param kwargs:
    :return: 登录成功后返回用户数据及token信息
    """
    url = Services.ip_port + '/api/v1/signLogin'
    method = 'POST'
    parametric_key = 'json'
    headers = {'Content-Type': 'application/json'}
    data = {
        'loginId': login_id,
        'passWord': password
    }

    return BaseRequest.send_request(method=method, url=url, parametric_key=parametric_key,data=data, headers=headers)

def user_token_refresh(token=None,**kwargs):
    """
    使用token进行有效期的延长
    :param token: 用户登录成功后的token信息
    :param kwargs:
    :return: 刷新成功后返回用户数据及token信息
    """
    url = Services.ip_port + '/api/v1/RefreshToken'
    method = 'POST'
    parametric_key = 'json'
    headers = {'Content-Type': 'application/json'}
    data = {
        'token': token
    }

    return BaseRequest.send_request(method=method, url=url, parametric_key=parametric_key,data=data, headers=headers)

def user_signout(token=None,**kwargs):
    """
    用户退出登录
    :param token: 用户登录成功后的token信息
    :param kwargs:
    :return: 重置用户token以失效，返回退出状态
    """
    url = Services.ip_port + '/api/v1/signout'
    method = 'POST'
    parametric_key = 'json'
    headers = {'Content-Type': 'application/json'}
    data = {
        'token': token
    }

    return BaseRequest.send_request(method=method, url=url, parametric_key=parametric_key,data=data, headers=headers)
