# -*- coding: utf-8 -*- 
# @Time      :  2022/8/19 10:47
# @Author    :  PythonKimo
# @File      :  __init__.py.py
# @Desc      :  
# -*- -*- -*- -*- -*- -*-
from . import user_managment

env = "DEV"

if env == "DEV":
    ip_port = 'http://127.0.0.1:5000'
elif env == "TEST":
    ip_port = 'http://www.4399.com'
else:
    ip_port = 'http://www.baidu.com'

