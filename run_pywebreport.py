# -*--*- coding: utf-8 -*--*-
# @Time    : 2021/10/16 20:26
# @Author  : PythonKimo
# @File    : run_pywebreport.py
# -*-*-*-*-*-*-*-*-*-*-*-*-*-
import os
import pytest
from Tools.log_output import get_logger
from Tools.data_modify import data_replace


logger = get_logger('logger')

def run():
    logger.info("""
      __ _ _ __ (_)  / \\  _   _| |_ __|_   _|__  ___| |_
     / _` | '_ \\| | / _ \\| | | | __/ _ \\| |/ _ \\/ __| __|
    | (_| | |_) | |/ ___ \\ |_| | || (_) | |  __/\\__ \\ |_
     \\__,_| .__/|_/_/   \\_\\__,_|\\__\\___/|_|\\___||___/\\__|
          |_|
          Starting      ...     ...     ...
        """)

    # 脚本执行方法一：
    args = ['.', '-q', '--report', 'Pywebreport_Result_and_Report/report.html']
    pytest.main(args)

    report_html_path = os.path.join(os.path.dirname(__file__), r"Pywebreport_Result_and_Report/report.html")
    data_replace(filepath=report_html_path,old_params="PyWebReport</div>",new_params="PythonKimo</div>")
    data_replace(filepath=report_html_path,old_params='id="dashboard" class="tabcontent" style="display: none', new_params='id="dashboard" class="tabcontent" style="display: block')

if __name__ == '__main__':
    '''
    run 脚本执行注意事项：
    1、执行方法前，先确保 API_Mock_By_Flask 文件下的 app.py 文件已经 run 起来了哦~
    2、生成的测试报告html文件路径： Pywebreport_Result_and_Report 文件下的 report.html
    '''
    run()

