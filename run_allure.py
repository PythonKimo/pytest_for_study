# -*--*- coding: utf-8 -*--*-
# @Time    : 2022/9/3 18:37
# @Author  : PythonKimo
# @File    : run_allure.py
# -*-*-*-*-*-*-*-*-*-*-*-*-*-
import os
import pytest
from Tools.log_output import get_logger

logger = get_logger('logger')

def run():
    logger.info("""
      __ _ _ __ (_)  / \\  _   _| |_ __|_   _|__  ___| |_
     / _` | '_ \\| | / _ \\| | | | __/ _ \\| |/ _ \\/ __| __|
    | (_| | |_) | |/ ___ \\ |_| | || (_) | |  __/\\__ \\ |_
     \\__,_| .__/|_/_/   \\_\\__,_|\\__\\___/|_|\\___||___/\\__|
          |_|
          Starting      ...     ...     ...
        """)

    ''' 
    ① 脚本执行方法一： 
    运行Pytest命令，执行脚本【可自定义测试文件/测试文件夹】
    如果这里执行报错：unrecognized arguments: --alluredir ./Allure_Result ，你需要 pip3 install allure-pytest
    '''
    pytest.main(['-sv','.\\testcases','--alluredir','./Allure_Result'])

    '''
    ② 脚本执行方法二：
    使用cmd命令执行脚本
    '''
    # cmd_command = f"pytest -v -s --alluredir=Allure_Result .\\testcases"
    # os.system(cmd_command)

    # 获取当前文件所在文件夹路径，也就是项目文件夹
    project_path = os.getcwd()
    # 测试报告 json 数据存储路径
    result_path = os.path.join(project_path, 'Allure_Result')
    # allure 测试报告存储路径
    report_path = os.path.join(project_path, 'Allure_Report')

    # 调用 cmd 命令，将 result_parh 的 json 数据转化为 Allure 报告并存储在 report_path 目录下
    cmd_command = 'allure generate ' + result_path + ' -o ' + report_path + ' --clean'
    logger.info("命令行生成报告命令：" + cmd_command)
    os.system(cmd_command)

    # 自动打开生成的 Allure 测试报告
    open_report_command = 'allure open -h 127.0.0.1 -p 6789 ./Allure_Report'
    os.system(open_report_command)
    logger.info(f'报告已生成')

if __name__ == '__main__':
    '''
    run 脚本执行注意事项：
    1、执行方法前，先确保 API_Mock_By_Flask 文件下的 app.py 文件已经 run 起来了哦~
    2、生成的测试报告html文件路径： Allure_Report 文件下的 index.html
    '''
    run()